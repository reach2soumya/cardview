﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CardViewController : Controller
    {
        public IActionResult Index()
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("https://recengine.intoday.in/recengine/at/getarticles");
                var parsedJson = JObject.Parse(json);
                var jsonData = parsedJson["data"].ToString();
                var new_string = JsonConvert.DeserializeObject<List<mCard>>(jsonData);
                return View(new_string);
            }
            //return View();
        }
    }
}
